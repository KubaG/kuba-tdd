const { expect } = require('chai')
const suma = require('./app')

// Cel: Aplikacja sumująca dwie liczby
// Red Green Refactor

it('sums up two identical numbers', () => {
  expect(suma(2, 2)).to.equal(4)
})

it('sums up two odd numbers', () => {
  expect(suma(3, 7)).to.equal(10)
})

it('sums up two negative numbers', () => {
  expect(suma(-3, -2)).to.equal(-5)
})

it('sums up one negative number and one positive', () => {
  expect(suma(-3, 2)).to.equal(-1)
})

it('does not sum up two letters', () => {
  // expect(suma("x", "y")).to.throw(new Error('Input has to be a number'))
  expect(suma("x", "y")).to.equal('zle')
})

it('does not sum up one letter and one number', () => {
  // expect(suma("x", y)).to.throw(new Error('Input has to be a number'))
  expect(suma("x", 3)).to.equal('zle')
})

it('does not sum up one letter and one number', () => {
  // expect(suma("x", y)).to.throw(new Error('Input has to be a number'))
  expect(suma(3, "y")).to.equal('zle')
})